//
//  AppDelegate.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/13/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@class MainTabBarViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, retain) MainTabBarViewController *mainTabBarCtrl;
@property (nonatomic, retain) User* curUser;
//@property (nonatomic, retain) NSString* user_id;
@property (nonatomic, retain) NSMutableArray* arrMyPlayList;
@property (nonatomic, retain) NSMutableArray* arrAccountType;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)showWaitingScreen:(NSString *)strText bShowText:(BOOL)bShowText;
- (void)hideWaitingScreen;

@end

AppDelegate* g_delegate;