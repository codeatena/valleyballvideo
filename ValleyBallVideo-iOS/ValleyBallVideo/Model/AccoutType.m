//
//  AccoutType.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/28/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "AccoutType.h"

@implementation AccoutType

@synthesize title, jsonKey, nLevel;

- (id) initWithTitle: (NSString*) _title
{
    self = [super init];
    
    if (self != nil) {
        title = _title;
    }
    
    return self;
}

+ (NSString*) getJsonKey: (int) nLevel
{
    if (nLevel == 15) return @"Platinum";
    if (nLevel == 16) return @"Gold_Member";
    if (nLevel == 17) return @"Silver_Member";
    if (nLevel == 18) return @"small_club";
    if (nLevel == 19) return @"large_club";
    
    return @"free";
}

@end