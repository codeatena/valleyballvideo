//
//  PlayListInf.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "PlayListInf.h"

@implementation PlayListInf

@synthesize title, count, isSelected, ID;

- (id) init
{
    self = [super init];
    
    if (self != nil) {
        ID = @"";
        title = @"";
        count = 0;
        isSelected = NO;
    }
    
    return self;
}

- (id) initWithTitle:(NSString*) _title COUNT:(int) _count
{
    self = [super init];
    
    if (self != nil) {
        title = _title;
        count = _count;
        isSelected = NO;
    }
    
    return self;
}

@end
