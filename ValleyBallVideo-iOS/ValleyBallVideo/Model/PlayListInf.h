//
//  PlayListInf.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayListInf : NSObject

@property(nonatomic, retain)        NSString* title;

@property(nonatomic)                NSString* ID;
@property(nonatomic)                int count;
@property(nonatomic)                BOOL isSelected;

- (id) initWithTitle:(NSString*) _title COUNT:(int) _count;

@end
