//
//  VideoCategory.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "VideoCategory.h"

@implementation VideoCategory

@synthesize name, arrVideo, searchKey, countVideo;

- (id) init
{
    self = [super init];
    
    if (self != nil) {
        searchKey = @"";
        name = @"";
        countVideo = 0;
        arrVideo = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (id) initWithName:(NSString*) _name
{
    self = [super init];
    
    if (self != nil) {
        searchKey = @"";
        name = _name;
        countVideo = 0;
        arrVideo = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (void) addVideo:(VideoInf*) video
{
    [arrVideo addObject:video];
    countVideo++;
}

@end
