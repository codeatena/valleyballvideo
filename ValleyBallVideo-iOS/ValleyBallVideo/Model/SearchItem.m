//
//  SearchItem.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/20/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "SearchItem.h"

@implementation SearchItem

@synthesize title, isChecked;

- (id) initWithTitle: (NSString*) _title
{
    self = [super init];
    
    if (self != nil) {
        title = _title;
        isChecked = NO;
    }

    return self;
}

@end
