//
//  AccoutType.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/28/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccoutType : NSObject

@property (nonatomic, retain)   NSString* title;
@property (nonatomic, retain)   NSString* jsonKey;
@property (nonatomic)           int nLevel;

- (id) initWithTitle: (NSString*) _title;
+ (NSString*) getJsonKey: (int) nLevel;

@end
