//
//  User.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 5/19/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, retain)   NSString* userID;
@property (nonatomic)           int     nLevel;

@end
