//
//  VideoInf.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoInf : NSObject

@property (nonatomic, retain) NSString* ID;
@property (nonatomic, retain) NSString* videoUrl;
@property (nonatomic, retain) NSString* videoImageUrl;
@property (nonatomic, retain) NSString* postImageUrl;
@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) NSString* postDate;

@property (nonatomic)       BOOL    isFree;
@property (nonatomic)       BOOL    isPlatinum;
@property (nonatomic)       BOOL    isSilver;
@property (nonatomic)       BOOL    isGold;
@property (nonatomic)       BOOL    isSmallClub;
@property (nonatomic)       BOOL    isLargeClub;

@property (nonatomic)       BOOL    isPremium;

@end
