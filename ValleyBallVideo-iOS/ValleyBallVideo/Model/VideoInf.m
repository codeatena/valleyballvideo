//
//  VideoInf.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "VideoInf.h"

@implementation VideoInf

@synthesize videoUrl, videoImageUrl, postImageUrl, title, desc, ID;
@synthesize isFree, isPlatinum, isGold, isSilver, isSmallClub, isLargeClub, isPremium, postDate;

- (id) init
{
    self = [super init];
    
    if (self != nil) {
        ID = @"";
        videoUrl = @"";
        videoImageUrl = @"";
        postImageUrl = @"";
        title = @"title";
        desc = @"desc";
        postDate = @"";
        
        isFree      = NO;
        isPlatinum  = NO;
        isSilver    = NO;
        isGold      = NO;
        isSmallClub = NO;
        isLargeClub = NO;
        
        isPremium   = NO;
    }
    
    return self;
}

@end
