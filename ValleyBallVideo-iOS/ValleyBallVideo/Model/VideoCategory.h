//
//  VideoCategory.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VideoInf.h"

@interface VideoCategory : NSObject

@property(nonatomic, retain)        NSString* name;
@property(nonatomic, retain)        NSMutableArray* arrVideo;
@property(nonatomic, retain)        NSString* searchKey;
@property(nonatomic)                int countVideo;

- (id) initWithName:(NSString*) _name;// test commit
- (void) addVideo:(VideoInf*) videoinf;// test commit again

@end
