//
//  SearchCategory.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/20/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "SearchCategory.h"

@implementation SearchCategory

@synthesize title, isExpand, isEdit, arrItem;

- (id) initWithTitle: (NSString*) _title
{
    self = [super init];
    
    if (self != nil) {
        title = _title;
        isExpand = NO;
        isExpand = NO;
        arrItem = [[NSMutableArray alloc] init];
    }
    
    return self;
}

@end