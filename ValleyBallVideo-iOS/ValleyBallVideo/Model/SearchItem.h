//
//  SearchItem.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/20/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchItem : NSObject

@property(nonatomic, retain) NSString* title;
@property(nonatomic) BOOL   isChecked;

- (id) initWithTitle: (NSString*) _title;

@end
