//
//  VideoTableViewCell.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/15/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "VideoTableViewCell.h"

@implementation VideoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setVideoInf:(VideoInf*) vidoeInf
{
    [lblTitle setText:vidoeInf.title];
    
}

@end
