//
//  VideoTableViewCell.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/15/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoInf.h"

@interface VideoTableViewCell : UITableViewCell
{
    IBOutlet    UIImageView*    imgThumbnail;
    IBOutlet    UILabel*        lblTitle;
}

- (void) setVideoInf:(VideoInf*) vidoeInf;

@end