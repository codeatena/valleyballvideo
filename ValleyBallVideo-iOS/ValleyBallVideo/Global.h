//
//  Define.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/25/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#ifndef ValleyBallVideo_Define_h
#define ValleyBallVideo_Define_h

#define API_URL   @"http://www.ivolleytech.com/api/"
#define LOGIN_FILE_PATH @"http://www.ivolleytech.com/api/login.php"
#define ALL_VIDEO_FILE_PATH @"http://www.ivolleytech.com/api/getpost.php"
#define GET_VIDEO_COUNT_PATH @"http://www.ivolleytech.com/api/getpostnumbers.php"

#define ADD_PLAYLIST_PATH @"http://www.ivolleytech.com/api/addplaylist.php"
#define GET_PLAYLIST_PATH @"http://www.ivolleytech.com/api/getplaylist.php"

#define GET_PLAYLIST_POST_PATH @"http://ivolleytech.com/api/getplaylistposts.php"

#define ADD_PLAYLIST_POST_PATH @"http://ivolleytech.com/api/addplaylistpost.php"
#define REMOVE_PLAYLIST_POST_PATH @"http://www.ivolleytech.com/api/removeplaylistpost.php"

#define GET_FEATURED_PATH @"http://www.ivolleytech.com/api/getfeatured.php"

#define SEARCH_URL        @"http://www.ivolleytech.com/api/getsearch.php"

enum LIST_TYPE {
    CATEGORY_LIST = 0,
    MY_PLAY_LIST,
    SEARCH_LIST
};

#endif