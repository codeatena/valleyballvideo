//
//  SearchViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchItem.h"
#import "SearchCategory.h"
#import "VideoListViewController.h"
#import "Global.h"

#define HEARDER_SWITCH_TAG_FIRST 100
#define HEADER_BUTTON_TAG_FIRST 200
#define HEADER_ARROW_TAG_FIRST 300

@interface SearchViewController ()

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self makeTableData];
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(onSearch:)];
    self.navigationItem.rightBarButtonItem = searchButton;
}

- (void) onSearch:(id) sender
{
    VideoListViewController* videoListViewCtrl = [[VideoListViewController alloc] initWithNibName:@"VideoListViewController" bundle:nil];
    videoListViewCtrl.nListType = SEARCH_LIST;

    videoListViewCtrl.nListType = SEARCH_LIST;
    
    NSString* url = [self getSearchUrl];
    NSLog(@"search url = %@", url);

    //    url = [NSString stringWithFormat:@"%@?playlist_id=8", GET_PLAYLIST_POST_PATH];
    [videoListViewCtrl setUrl:url VIDEO_COUNT:100];
    [self.navigationController pushViewController:videoListViewCtrl animated:YES];
}

- (void) makeTableData
{
    NSMutableArray* arrSection = [[NSMutableArray alloc] init];
    
    NSMutableArray* arrFormat = [[NSMutableArray alloc] initWithObjects:@"Video", @"Article", @"Podcast", @"Worksheet", nil];
    NSMutableArray* arrSport = [[NSMutableArray alloc] initWithObjects:@"Beach", @"Indoor", nil];
    NSMutableArray* arrPresentation = [[NSMutableArray alloc] initWithObjects:@"Drill Demonstrations", @"Skill Breakdowns", @"Game Strategy", @"Interview", @"Classroom Lecture", @"Other", nil];
    NSMutableArray* arrTopic = [[NSMutableArray alloc] initWithObjects:@"Serving", @"Setting", @"Hitting", @"Passing", @"Blocking", @"Warm Ups", @"Individual Defense", @"International Volleyball", @"Club Volleyball", @"Team Offense", @"Team Defense", @"Practice", @"Strength Training", @"Nutrition", @"Philosophy", @"Transitions", @"Stats", @"Sports Psychology", @"Mental Training", @"Tryouts", @"Parents", @"Rotations", @"Recruiting", @"Serve Receive", @"Performance Training", nil];
    NSMutableArray* arrCompletionLevel = [[NSMutableArray alloc] initWithObjects:@"Beginner", @"Middle School", @"High School", @"College", nil];
    NSMutableArray* arrLengthOfContent = [[NSMutableArray alloc] initWithObjects:@"0 - 5 minutes", @"5 - 10 minutes", @"10 - 20 minutes", @"30 - 60 minutes", @"60+ Minutes", nil];
    
    [arrSection addObject:arrFormat];
    [arrSection addObject:arrSport];
    [arrSection addObject:arrPresentation];
    [arrSection addObject:arrTopic];
    [arrSection addObject:arrCompletionLevel];
    [arrSection addObject:arrLengthOfContent];
    
    NSMutableArray* arrSectionTitle = [[NSMutableArray alloc] initWithObjects:@"Format", @"Sport", @"Presentation", @"Topic", @"Competition Level", @"Length of Content", nil];
    
    arrSearchCategory = [[NSMutableArray alloc] init];
    int k = 0;
    for (NSMutableArray* arr in arrSection) {
        NSString* searchTitle = [arrSectionTitle objectAtIndex:k];
        SearchCategory* searchCategory = [[SearchCategory alloc] initWithTitle:searchTitle];
        
        for (NSString* itemTitle in arr) {
            SearchItem* searchItem = [[SearchItem alloc] initWithTitle:itemTitle];
            [searchCategory.arrItem addObject:searchItem];
        }
        k++;
        [arrSearchCategory addObject:searchCategory];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrSearchCategory count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SearchCategory* category = [arrSearchCategory objectAtIndex:section];
    
    if (category.isExpand == YES) {
        return [category.arrItem count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"acell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"acell"];
    }

    SearchCategory* category = [arrSearchCategory objectAtIndex:indexPath.section];
    SearchItem* item = [category.arrItem objectAtIndex:indexPath.row];
    
    if (item.isChecked == YES) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.textLabel.text = item.title;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0, 1, 320, 48)];
    button.tag = HEADER_BUTTON_TAG_FIRST + section;
    [button addTarget:self action:@selector(changeItem:) forControlEvents:UIControlEventTouchDown];
    [button setBackgroundColor:[UIColor grayColor]];
    [view addSubview:button];
    
    UILabel* lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 200, 40)];
    [view setBackgroundColor:[UIColor blackColor]];
    
    SearchCategory* category = [arrSearchCategory objectAtIndex:section];
    lblTitle.text = category.title;
    [view addSubview:lblTitle];
    
    UISwitch* selSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    selSwitch.center = CGPointMake(280, 25);
    [view addSubview:selSwitch];
    selSwitch.tag = HEARDER_SWITCH_TAG_FIRST + section;
    selSwitch.on = category.isEdit;
    
    UIImageView* arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 10, 20, 30)];
    if (category.isExpand) {
        arrowImageView.image = [UIImage imageNamed:@"arrow_up.png"];
    } else {
        arrowImageView.image = [UIImage imageNamed:@"arrow_down.png"];
    }
    arrowImageView.tag = HEADER_ARROW_TAG_FIRST + section;
    [view addSubview:arrowImageView];
    
    [selSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    return view;
}

- (void) changeItem:(id) sender
{
    UIButton* button = (UIButton*) sender;
    int curIndex = button.tag - HEADER_BUTTON_TAG_FIRST;
    
    SearchCategory* category = [arrSearchCategory objectAtIndex:curIndex];
//    category.isExpand = !category.isExpand;
    NSLog(@"button %d", curIndex);
    [myTableView reloadData];
    
    if (category.isExpand == YES) {
        [self collapseSection:curIndex];
    } else {
        if (category.isEdit == YES) {
            [self expandSection:curIndex];
        }
    }
}

- (void) changeSwitch:(id)sender
{
    UISwitch* curSwitch = (UISwitch*) sender;
    int curIndex = curSwitch.tag - HEARDER_SWITCH_TAG_FIRST;
    NSLog(@"switch %d", curIndex);
    
    SearchCategory* category = [arrSearchCategory objectAtIndex:curIndex];
    category.isEdit = curSwitch.on;
    
    for (SearchItem* item in category.arrItem) {
        item.isChecked = NO;
    }
    
    [myTableView reloadData];
    
    if (category.isEdit == YES) {
        if (category.isExpand == NO) {
            [self expandSection: curIndex];
        }
    }
    if (category.isEdit == NO) {
        if (category.isExpand == YES) {
            [self collapseSection:curIndex];
        }
    }
}

- (void) expandSection:(int) section
{
    [myTableView beginUpdates];

    SearchCategory* category = [arrSearchCategory objectAtIndex:section];
    category.isExpand = YES;

    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < category.arrItem.count; i++) {
        NSIndexPath* indexPathToInsert = [NSIndexPath indexPathForRow:i inSection:section];
        [rowToInsert addObject:indexPathToInsert];
    }
    
    [myTableView insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationTop];
   
    [myTableView endUpdates];
    
//    UIView* view = (UIView *) [self tableView:myTableView viewForHeaderInSection:section];
//    UIImageView* arrowImageView = (UIImageView*) [view viewWithTag:HEADER_ARROW_TAG_FIRST + section];
//    arrowImageView.image = [UIImage imageNamed:@"arrow_up.png"];
    
    [myTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void) collapseSection:(int) section
{
    [myTableView beginUpdates];
    
    SearchCategory* category = [arrSearchCategory objectAtIndex:section];
    category.isExpand = NO;

    NSMutableArray* rowsToDelete = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < category.arrItem.count; i++) {
        NSIndexPath* indexPathToDelete = [NSIndexPath indexPathForRow:i inSection:section];
        [rowsToDelete addObject:indexPathToDelete];
    }
    

    [myTableView deleteRowsAtIndexPaths:rowsToDelete withRowAnimation:UITableViewRowAnimationTop];
    
    [myTableView endUpdates];
    
//    UIView* view = (UIView *) [self tableView:myTableView viewForHeaderInSection:section];
//    UIImageView* arrowImageView = (UIImageView*) [view viewWithTag:HEADER_ARROW_TAG_FIRST + section];
//    arrowImageView.image = [UIImage imageNamed:@"arrow_down.png"];
    
    [myTableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    
//    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    SearchCategory* category = [arrSearchCategory objectAtIndex:indexPath.section];
    SearchItem* item = [category.arrItem objectAtIndex:indexPath.row];
    
    item.isChecked = !item.isChecked;
    
    BOOL isSectionChecked = NO;
    for (SearchItem* item in category.arrItem) {
        if (item.isChecked == YES) {
            isSectionChecked = YES;
            break;
        }
    }
    
    category.isEdit = isSectionChecked;
    
    [tableView reloadData];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString*) getSearchUrl
{
    NSString* url = SEARCH_URL;

    BOOL isFirstSection = NO;
    for (SearchCategory* category in arrSearchCategory) {
        BOOL isSectionChecked = NO;
//        NSString* sectionFilter = @"";
        for (SearchItem* item in category.arrItem) {
            if (item.isChecked) {
                if (!isSectionChecked) {
                    
                    NSString* categorytitle = [category.title lowercaseString];
                    if (!isFirstSection) {
                        url = [NSString stringWithFormat:@"%@?%@=%@", url, categorytitle, item.title];
                        isFirstSection = YES;
                    } else {
                        url = [NSString stringWithFormat:@"%@&%@=%@", url, categorytitle, item.title];
                    }
                } else {
                    url = [NSString stringWithFormat:@"%@--%@", url, item.title];
                }
                isSectionChecked = YES;
            }
        }
    }
    
    return url;
}

@end
