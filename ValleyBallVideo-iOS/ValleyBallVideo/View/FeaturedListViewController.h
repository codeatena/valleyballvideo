//
//  FeaturedListViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/13/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoInf.h"

@interface FeaturedListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate>
{
    NSMutableArray* arrFeatured;
    
    IBOutlet UITableView* myTableView;
    
    VideoInf* latestFeatureVideoInf;
    
    NSURLConnection* urlConnection;
    NSMutableData* receivedData;
}

- (void) addPlayList: (VideoInf*) videoInf;

@end
