//
//  SubmitViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/27/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+DataSourceBlocks.h"
@class TableViewWithBlock;

@interface SubmitViewController : UIViewController
{
    IBOutlet UIScrollView* scrollView;
    
    IBOutlet UITextField*           txtAccountType;
    IBOutlet TableViewWithBlock*    cmbAccountType;
    IBOutlet UIButton*              btnDropDownAccountType;
    
    IBOutlet UITextField*           txtFirstName;
    IBOutlet UITextField*           txtLastName;
    IBOutlet UITextField*           txtAddress;
    IBOutlet UITextField*           txtCity;
    IBOutlet UITextField*           txtStateProvince;
    IBOutlet UITextField*           txtZipCode;
    
    IBOutlet UITextField*           txtCountry;
    IBOutlet TableViewWithBlock*    cmbCountry;
    IBOutlet UIButton*              btnDropDownCountry;
    
    IBOutlet UITextField*           txtEmail;
    IBOutlet UITextField*           txtPhone;
    
    IBOutlet UITextField*           txtCardNumber;
    IBOutlet UITextField*           txtNumberOnCard;
    
    IBOutlet UITextField*           txtExpirationMonth;
    IBOutlet TableViewWithBlock*    cmbExpirationMonth;
    IBOutlet UIButton*              btnDropDownExpirationMonth;
    
    IBOutlet UITextField*           txtExpirationYear;
    IBOutlet TableViewWithBlock*    cmbExpirationYear;
    IBOutlet UIButton*              btnDropDownExpirationYear;
    
    IBOutlet UITextField*           txtCVV;
    
    BOOL        isOpenAccountType;
    BOOL        isOpenCountry;
    BOOL        isOpenExpirationMonth;
    BOOL        isOpenExpirationYear;
}

- (IBAction)onChangeAccountType:(id)sender;
- (IBAction)onChangeCountry:(id)sender;
- (IBAction)onChangeExpirationMonth:(id)sender;
- (IBAction)onChangeExpirationYear:(id)sender;

@end
