//
//  RegisterViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

- (IBAction) onSilver:(id)sender;
- (IBAction) onGlod:(id)sender;
- (IBAction) onPlatnium:(id)sender;
- (IBAction) onCoach:(id)sender;
- (IBAction) onCoachPlus:(id)sender;

@end
