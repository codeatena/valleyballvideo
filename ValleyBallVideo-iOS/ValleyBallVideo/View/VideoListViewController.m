//
//  VideoListViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "VideoListViewController.h"
#import "VideoTableViewCell.h"
#import "VideoPlayerViewController.h"
#import "ChoosePlayListViewController.h"
#import "AppDelegate.h"
#import "Global.h"

@interface VideoListViewController ()

@end

@implementation VideoListViewController

@synthesize nListType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UINib *nibVideoCell = [UINib nibWithNibName:@"VideoTableViewCell" bundle:nil];
    [myTableView registerNib: nibVideoCell forCellReuseIdentifier:@"cell"];
    
    nCurrentPage = 0;
    arrVideoInf = [[NSMutableArray alloc] init];
    [self requestWithPage:nCurrentPage];
}

- (void) viewWillAppear:(BOOL)animated
{
    [myTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setVideoArray:(NSMutableArray*) _arrVideoInf;
{
    arrVideoInf = [[NSMutableArray alloc] init];
    arrVideoInf = _arrVideoInf;
}

- (void) requestWithPage:(int) n
{
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate showWaitingScreen:@"Please wait.." bShowText:YES];
    
    NSString* url = searchUrl;
    if (n > 0) {
        url = [NSString stringWithFormat:@"%@&start=%d", url, n * 20];
    }
    
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"Search Url = %@", url);
    
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
    NSMutableString *post_data     = [[NSMutableString alloc] init];
    receivedData = [NSMutableData dataWithCapacity: 0];
    
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
    urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSLog(@"Video count = %d", arrVideoInf.count);
    return [arrVideoInf count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *videoCellIdentifier = @"cell";
    
    VideoTableViewCell *videoCell = (VideoTableViewCell*) [tableView dequeueReusableCellWithIdentifier: videoCellIdentifier forIndexPath:indexPath];
    VideoInf* videoInf = [arrVideoInf objectAtIndex:indexPath.row];
    videoCell.parentViewCtrl = self;

    [videoCell setVideoInf:videoInf];
    
    return videoCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideoPlayerViewController* videoPlayerViewCtrl = nil;
    VideoInf* videoInf = [arrVideoInf objectAtIndex:indexPath.row];
    BOOL isCanPlay = YES;

    if (videoInf.isPremium) {
        isCanPlay = NO;
        if (g_delegate.curUser != nil) {
            isCanPlay = NO;
            int nLevel = g_delegate.curUser.nLevel;
            if (nLevel == 2 && videoInf.isFree) {
                isCanPlay = YES;
            }
            if (nLevel == 13 && videoInf.isPlatinum) {
                isCanPlay = YES;
            }
            if (nLevel == 5 && videoInf.isGold) {
                isCanPlay = YES;
            }
            if (nLevel == 4 && videoInf.isSilver) {
                isCanPlay = YES;
            }
            if (nLevel == 18 && videoInf.isSmallClub) {
                isCanPlay = YES;
            }
            if (nLevel == 19 && videoInf.isLargeClub) {
                isCanPlay = YES;
            }
        }
    }
    
    if (isCanPlay) {
        
        videoPlayerViewCtrl = [[VideoPlayerViewController alloc] initWithNibName:@"VideoPlayerViewController" bundle:nil];
        [self.navigationController pushViewController:videoPlayerViewCtrl animated:YES];
        
        [videoPlayerViewCtrl setVideoInf:videoInf];
        [tableView deselectRowAtIndexPath: indexPath animated: YES];
    } else {
        [self showLoginAlert];
    }
}

- (void) showLoginAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                    message:@"Your account is not enough to see this video."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) addPlayList: (VideoInf*) videoInf
{
    ChoosePlayListViewController* choosePlayListViewCtrl = [[ChoosePlayListViewController alloc] initWithNibName:@"ChoosePlayListViewController" bundle:nil];
    
    choosePlayListViewCtrl.myVideoInf = videoInf;
    
    UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:choosePlayListViewCtrl];
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

- (void) setUrl:(NSString*) url VIDEO_COUNT:(int) count;
{
    searchUrl = url;
    videoCount = count;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnection) {
        [receivedData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnection) {
        // use data from data_users
        
        NSString *myString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        NSLog(@"video list result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        NSArray* arrJsonVideo = [jsonObject objectForKey:@"message"];
        
        if ([arrJsonVideo isKindOfClass:[NSString class]]) {
            
        } else {
//            arrVideoInf = [[NSMutableArray alloc] init];
            
            for (NSDictionary* jsonVideo in arrJsonVideo) {
                
                VideoInf* video = [[VideoInf alloc] init];
                video.ID = [jsonVideo objectForKey:@"ID"];
                video.title = [jsonVideo objectForKey:@"post_title"];
                video.desc = [jsonVideo objectForKey:@"description"];
                if (video.desc == nil) video.desc = @"";
                video.videoImageUrl = [jsonVideo objectForKey:@"video_thumbnail"];
                video.postImageUrl = [jsonVideo objectForKey:@"post_thumbnail"];
                video.videoUrl = [jsonVideo objectForKey:@"video_url"];
                video.postDate = [jsonVideo objectForKey:@"post_date"];

                if ([video.postImageUrl isKindOfClass:[NSNull class]]) {
                    NSLog(@"nil");
                    video.postImageUrl = @"";
                }
                
                video.isFree = (BOOL) [jsonVideo valueForKey:@"free"];
                video.isPlatinum = (BOOL) [jsonVideo valueForKey:@"Platinum"];
                video.isSilver = (BOOL) [jsonVideo valueForKey:@"Silver_Member"];
                video.isGold = (BOOL) [jsonVideo valueForKey:@"Gold_Member"];
                video.isSmallClub = (BOOL) [jsonVideo valueForKey:@"small_club"];
                video.isLargeClub = (BOOL) [jsonVideo valueForKey:@"large_club"];
                
                if (video.isFree || video.isPlatinum || video.isSilver || video.isGold || video.isSmallClub || video.isLargeClub) {
                    video.isPremium = YES;
                }
                
                [arrVideoInf addObject:video];
            }
//            NSLog(@"video count = %d", [arrJsonVideo count]);
        }

        [myTableView reloadData];
        AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        [delegate hideWaitingScreen];
    }
    // do the same for cards connection
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // Release the connection and the data object
    // by setting the properties (declared elsewhere)
    // to nil.  Note that a real-world app usually
    // requires the delegate to manage more than one
    // connection at a time, so these lines would
    // typically be replaced by code to iterate through
    // whatever data structures you are using.
    urlConnection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)scrollViewDidEndDragging:(UIScrollView *)aScrollView
                  willDecelerate:(BOOL)decelerate{
    
    if (nListType == CATEGORY_LIST) {
        CGPoint offset = aScrollView.contentOffset;
        CGRect bounds = aScrollView.bounds;
        CGSize size = aScrollView.contentSize;
        UIEdgeInsets inset = aScrollView.contentInset;
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
        
        float reload_distance = 50;
        if(y > h + reload_distance) {
            NSLog(@"load more rows");
            nCurrentPage++;
            if ((nCurrentPage) * 20 > videoCount) return;
            [self requestWithPage:nCurrentPage];
        }
    }
}

@end
