//
//  LoginViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<NSURLConnectionDelegate>
{
    IBOutlet UITextField* txtUserName;
    IBOutlet UITextField* txtPassword;
    
    NSURLConnection* urlConnection;
}

- (IBAction)onRegister:(id)sender;
- (IBAction)onLogin:(id)sender;
- (IBAction)onDone:(id)sender;

@end
