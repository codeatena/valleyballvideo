//
//  MainTabBarViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/13/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "MainTabBarViewController.h"
#import "FeaturedListViewController.h"
#import "AllCategoryListViewController.h"
#import "MyPlayListViewController.h"
#import "SearchViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Common.h"

@interface MainTabBarViewController ()

@end

@implementation MainTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    FeaturedListViewController* featuredListViewCtrl = nil;
    AllCategoryListViewController* allCategoryListViewCtrl = nil;
    MyPlayListViewController* myPlayListViewCtrl = nil;
    SearchViewController* searchViewCtrl = nil;
    LoginViewController* loginViewCtrl = nil;
    
    featuredListViewCtrl = [[FeaturedListViewController alloc] initWithNibName:@"FeaturedListViewController" bundle:nil];
    allCategoryListViewCtrl = [[AllCategoryListViewController alloc] initWithNibName:@"AllCategoryListViewController" bundle:nil];
    myPlayListViewCtrl = [[MyPlayListViewController alloc] initWithNibName:@"MyPlayListViewController" bundle:nil];
    searchViewCtrl = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    loginViewCtrl = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    UINavigationController* navFeatured = [[UINavigationController alloc] initWithRootViewController:featuredListViewCtrl];
    UINavigationController* navAllVideo = [[UINavigationController alloc] initWithRootViewController:allCategoryListViewCtrl];
//    UINavigationController* navMyPlayList = [[UINavigationController alloc] initWithRootViewController:myPlayListViewCtrl];
    UINavigationController* navLogin = [[UINavigationController alloc] initWithRootViewController:loginViewCtrl];
    UINavigationController* navSearch = [[UINavigationController alloc] initWithRootViewController:searchViewCtrl];

    NSMutableArray* arrViewCtrl = [[NSMutableArray alloc] initWithObjects: navFeatured, navAllVideo, navLogin, navSearch, nil];
    self.viewControllers = arrViewCtrl;
    
    UITabBar *tabBar = self.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
//    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    tabBarItem1.title = @"Featured";
    tabBarItem2.title = @"All Videos";
    tabBarItem3.title = @"Premium";
    tabBarItem4.title = @"Search";
//    tabBarItem5.title = @"Search";

//    UIImage* image = [UIImage imageNamed:@"icons_Art.png"];
//    CGSize newSize = CGSizeMake(45, 35);
//    UIGraphicsBeginImageContext(newSize);
//    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
    tabBarItem1.image = [Common resizeImage:[UIImage imageNamed:@"tab_featured.png"] newSize:CGSizeMake(28, 28)];
    tabBarItem2.image = [Common resizeImage:[UIImage imageNamed:@"tab_videos.png"] newSize:CGSizeMake(25, 25)];
    tabBarItem3.image = [Common resizeImage:[UIImage imageNamed:@"tab_premium.png"] newSize:CGSizeMake(25, 25)];
    tabBarItem4.image = [Common resizeImage:[UIImage imageNamed:@"tab_search.png"] newSize:CGSizeMake(20, 20)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    NSLog(@"%d", self.selectedIndex);
    
//    g_delegate.window.rootViewController.
    
    return UIInterfaceOrientationMaskPortrait;
}

@end
