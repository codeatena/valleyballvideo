//
//  SearchViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView* myTableView;
    NSMutableArray*     arrSearchCategory;
}

@end
