//
//  ChoosePlayListViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ChoosePlayListViewController.h"
#import "ChoosePlayListTableViewCell.h"
#import "AppDelegate.h"
#import "Global.h"

@interface ChoosePlayListViewController ()

@end

@implementation ChoosePlayListViewController

@synthesize myVideoInf;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Choose Playlists";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self makePlayList];
    
    UINib *niCategoryCell = [UINib nibWithNibName:@"ChoosePlayListTableViewCell" bundle:nil];
    [myTableView registerNib: niCategoryCell forCellReuseIdentifier:@"ChoosePlayListIdentifier"];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(onDone:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    arrPlayList = g_delegate.arrMyPlayList;
}

- (void) onDone:(id) sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) makePlayList
{
    arrPlayList = [[NSMutableArray alloc] init];
    PlayListInf* playList = nil;
    
    playList = [[PlayListInf alloc] initWithTitle:@"My PlayList" COUNT:3];
    [arrPlayList addObject:playList];
    
    playList = [[PlayListInf alloc] initWithTitle:@"CEO/Analytics" COUNT:4];
    [arrPlayList addObject:playList];
    
    playList = [[PlayListInf alloc] initWithTitle:@"Programming" COUNT:2];
    [arrPlayList addObject:playList];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
//    NSLog(@"Play List Count = %d", arrPlayList.count);
    return [arrPlayList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"ChoosePlayListIdentifier";
    
    ChoosePlayList_TableViewCell *playListCell = (ChoosePlayList_TableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    PlayListInf* playListInf = [arrPlayList objectAtIndex:indexPath.row];
    
    [playListCell setPlayListInf:playListInf];
    
    return playListCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlayListInf* playListInf = [arrPlayList objectAtIndex:indexPath.row];
    
    if (playListInf.isSelected) {
        NSString* url = [NSString stringWithFormat:@"%@?playlist_id=%@&post_id=%@", REMOVE_PLAYLIST_POST_PATH, playListInf.ID, myVideoInf.ID];
        
        NSLog(@"remove playlist post url = %@", url);
        NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
        NSMutableString *post_data     = [[NSMutableString alloc] init];
        receivedRemovePostData = [NSMutableData dataWithCapacity: 0];
        
        [urlRequest setURL:[NSURL URLWithString:url]];
        [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPMethod:@"GET"];
        [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
        
        //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
        urlRemovePostConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        [g_delegate showWaitingScreen:@"Please wait.." bShowText:YES];
        
        playListInf.isSelected = NO;
        playListInf.count--;
    } else {
        NSString* url = [NSString stringWithFormat:@"%@?userid=%@&playlist_id=%@&post_id=%@", ADD_PLAYLIST_POST_PATH, g_delegate.curUser.userID, playListInf.ID, myVideoInf.ID];
        
        NSLog(@"add playlist post url = %@", url);
        NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
        NSMutableString *post_data     = [[NSMutableString alloc] init];
        receivedAddPostData = [NSMutableData dataWithCapacity: 0];
        
        [urlRequest setURL:[NSURL URLWithString:url]];
        [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPMethod:@"GET"];
        [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
        
        //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
        urlAddPostConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        [g_delegate showWaitingScreen:@"Please wait.." bShowText:YES];
        
        playListInf.count++;
        playListInf.isSelected = YES;
    }
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    [myTableView reloadData];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlAddPostConnection) {
        [receivedAddPostData appendData:data];
    }
    if (connection == urlRemovePostConnection) {
        [receivedRemovePostData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlAddPostConnection) {
        
        NSString *myString = [[NSString alloc] initWithData:receivedAddPostData encoding:NSUTF8StringEncoding];
        
        NSLog(@"add post result = %@", myString);
        
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedAddPostData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        
        [g_delegate hideWaitingScreen];

    }
    if (connection == urlRemovePostConnection) {
        
        NSString *myString = [[NSString alloc] initWithData:receivedRemovePostData encoding:NSUTF8StringEncoding];
        
        NSLog(@"remove post result = %@", myString);
        
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedRemovePostData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        
        [g_delegate hideWaitingScreen];
    }
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    urlAddPostConnection = nil;
    urlRemovePostConnection = nil;
    
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}


@end
