//
//  MyPlayListViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPlayListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, NSURLConnectionDelegate>
{
    IBOutlet    UITableView* myTableView;
    NSMutableArray*         arrPlayList;
    
    NSURLConnection* urlConnectionAddPlayList;
    NSURLConnection* urlConnectionGetPlayList;
    
    NSMutableData* receivedDataAddPlayList;
    NSMutableData* receivedDataGetPlayList;
}

- (IBAction) onNewPlayList:(id)sender;

@end
