//
//  MyPlayListViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "MyPlayListViewController.h"
#import "PlayListInf.h"
#import "PlayListTableViewCell.h"
#import "VideoListViewController.h"
#import "Global.h"
#import "AppDelegate.h"

@interface MyPlayListViewController ()

@end

@implementation MyPlayListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"My PlayLists";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self makePlayList];
    
    UINib *niCategoryCell = [UINib nibWithNibName:@"PlayListTableViewCell" bundle:nil];
    [myTableView registerNib: niCategoryCell forCellReuseIdentifier:@"PlayListTableViewCell"];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [self getPlayList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) makePlayList
{
    arrPlayList = [[NSMutableArray alloc] init];
//    PlayListInf* playList = nil;
//    
//    playList = [[PlayListInf alloc] initWithTitle:@"My PlayList" COUNT:3];
//    [arrPlayList addObject:playList];
//    
//    playList = [[PlayListInf alloc] initWithTitle:@"CEO/Analytics" COUNT:4];
//    [arrPlayList addObject:playList];
//
//    playList = [[PlayListInf alloc] initWithTitle:@"Programming" COUNT:2];
//    [arrPlayList addObject:playList];
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSLog(@"Play List Count = %d", arrPlayList.count);
    return [arrPlayList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *videoCellIdentifier = @"PlayListTableViewCell";
    
    PlayListTableViewCell *playListCell = (PlayListTableViewCell*) [tableView dequeueReusableCellWithIdentifier: videoCellIdentifier forIndexPath:indexPath];
    PlayListInf* playListInf = [arrPlayList objectAtIndex:indexPath.row];
    
    [playListCell setPlayListInf:playListInf];
    
    return playListCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideoListViewController* videoListViewCtrl = nil;
    
    videoListViewCtrl = [[VideoListViewController alloc] initWithNibName:@"VideoListViewController" bundle:nil];
    
    PlayListInf* playList = [arrPlayList objectAtIndex: indexPath.row];
    [videoListViewCtrl setTitle:playList.title];
    [self.navigationController pushViewController:videoListViewCtrl animated:YES];
    
    [videoListViewCtrl setTitle:playList.title];
    videoListViewCtrl.nListType = MY_PLAY_LIST;

    NSString* url = [NSString stringWithFormat:@"%@?playlist_id=%@", GET_PLAYLIST_POST_PATH, playList.ID];
//    url = [NSString stringWithFormat:@"%@?playlist_id=8", GET_PLAYLIST_POST_PATH];
    [videoListViewCtrl setUrl:url VIDEO_COUNT:playList.count];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (IBAction) onNewPlayList:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"New Playlist"
                              message:@""
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Create", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    /* Display a numerical keypad for this text field */
    UITextField *textField = [alertView textFieldAtIndex:0];
    
    [alertView show];
}

- (void) getPlayList
{
    NSString* url = [NSString stringWithFormat:@"%@?userid=%@", GET_PLAYLIST_PATH, g_delegate.curUser.userID];
    
    NSLog(@"get playlist url = %@", url);
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
    NSMutableString *post_data     = [[NSMutableString alloc] init];
    receivedDataGetPlayList = [NSMutableData dataWithCapacity: 0];
    
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"GET"];
    [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
    urlConnectionGetPlayList = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [g_delegate showWaitingScreen:@"Please wait.." bShowText:YES];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%d", buttonIndex);
    if (buttonIndex == 1) {
        UITextField* textField = [alertView textFieldAtIndex:0];
        NSString* title = textField.text;
        
//        PlayListInf* newPlayList = [[PlayListInf alloc] initWithTitle:title COUNT:0];
//        [arrPlayList addObject:newPlayList];
//        [myTableView reloadData];

        NSString* url = [NSString stringWithFormat:@"%@?userid=%@&playlist_title=%@", ADD_PLAYLIST_PATH, g_delegate.curUser.userID, title];
        
        NSLog(@"add playlist url = %@", url);
        NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
        NSMutableString *post_data     = [[NSMutableString alloc] init];
        receivedDataAddPlayList = [NSMutableData dataWithCapacity: 0];
        
        [urlRequest setURL:[NSURL URLWithString:url]];
        [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPMethod:@"GET"];
        [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
        
        //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
        urlConnectionAddPlayList = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
        [g_delegate showWaitingScreen:@"Please wait.." bShowText:YES];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnectionAddPlayList) {
        [receivedDataAddPlayList appendData:data];
    }
    if (connection == urlConnectionGetPlayList) {
        [receivedDataGetPlayList appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnectionAddPlayList) {
        // use data from data_users
        NSString *myString = [[NSString alloc] initWithData:receivedDataAddPlayList encoding:NSUTF8StringEncoding];
        
        NSLog(@"add playlist result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedDataAddPlayList
                         options:NSJSONReadingAllowFragments
                         error:&error];
        
        [g_delegate hideWaitingScreen];
        [self getPlayList];
    }
    if (connection == urlConnectionGetPlayList) {
        NSString *myString = [[NSString alloc] initWithData:receivedDataGetPlayList encoding:NSUTF8StringEncoding];
        
        NSLog(@"get playlist result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedDataGetPlayList
                         options:NSJSONReadingAllowFragments
                         error:&error];
        if (jsonObject != nil && error == nil){
//            NSLog(@"Successfully deserialized...");
            
            //check if the country code was valid
            NSArray* arrJsonPlayList = [jsonObject objectForKey:@"message"];
            
            if ([arrJsonPlayList isKindOfClass:[NSString class]]) {
                
            } else {
                
//                g_delegate.arrMyPlayList = [[NSMutableArray alloc] init];
                arrPlayList = [[NSMutableArray alloc] init];
                for (NSDictionary* jsonPlayList in arrJsonPlayList) {
                    PlayListInf* playListInf = [[PlayListInf alloc] init];
                    
                    playListInf.ID = [jsonPlayList objectForKey:@"id"];
                    playListInf.title = [jsonPlayList objectForKey:@"title"];
                    playListInf.count = [(NSString*) [jsonPlayList objectForKey:@"total_videos"] intValue];
//                    NSLog(@"%d", playListInf.count);
                    [arrPlayList addObject:playListInf];
                }
                
                g_delegate.arrMyPlayList = arrPlayList;
                NSLog(@"global playlist count = %d", g_delegate.arrMyPlayList.count);
                
                [myTableView reloadData];
            }
        }
        
        [g_delegate hideWaitingScreen];
    }
    // do the same for cards connection
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    // Release the connection and the data object
    // by setting the properties (declared elsewhere)
    // to nil.  Note that a real-world app usually
    // requires the delegate to manage more than one
    // connection at a time, so these lines would
    // typically be replaced by code to iterate through
    // whatever data structures you are using.
    if (connection == urlConnectionAddPlayList) {
        urlConnectionAddPlayList = nil;
    }
    if (connection == urlConnectionGetPlayList) {
        urlConnectionGetPlayList = nil;
    }
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

@end
