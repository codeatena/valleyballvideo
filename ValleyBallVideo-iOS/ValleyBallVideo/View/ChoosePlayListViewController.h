//
//  ChoosePlayListViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoInf.h"

@interface ChoosePlayListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate>
{
    IBOutlet UITableView* myTableView;
    NSMutableArray*         arrPlayList;
    
    NSURLConnection* urlAddPostConnection;
    NSURLConnection* urlRemovePostConnection;

    NSMutableData* receivedAddPostData;
    NSMutableData* receivedRemovePostData;
}

@property (nonatomic, retain) VideoInf* myVideoInf;

@end