//
//  VideoPlayerViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/17/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "VideoPlayerViewController.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "AppDelegate.h"

@interface VideoPlayerViewController ()

@end

@implementation VideoPlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Video Details";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    lblTitle.text = myVideoInf.title;
    lblDesc.text = myVideoInf.desc;
    
    NSString* postImageUrl = myVideoInf.postImageUrl;
    
    [imgThumbnail setImageWithURL:[NSURL URLWithString:postImageUrl]
                 placeholderImage:[UIImage imageNamed:postImageUrl]
      usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [lblDesc sizeToFit];
    CGRect rect = lblDesc.frame;
    NSLog(@"x=%f, y=%f, w=%f, h=%f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    
    NSLog(@"Video Url = %@", myVideoInf.videoUrl);
    [self loadWebThumbnail];
    [scrollView setContentSize:CGSizeMake(320, rect.origin.y + rect.size.height + 5)];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:myVideoInf.postDate];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"HH:mm, MMMM yyyy"];
    NSString *newDateString = [dateFormatter2 stringFromDate:date];
    
    lblDate.text = [NSString stringWithFormat:@"Posted %@", newDateString];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MPMoviePlayerPlaybackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:nil];
}

- (void)MPMoviePlayerPlaybackStateDidChange:(NSNotification *)notification
{
    NSLog(@"notification");
}

- (void) loadWebThumbnail
{
    NSString *urlString = myVideoInf.videoUrl;
    //
    //2
    NSURL *url = [NSURL URLWithString:urlString];
    
    //3
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    //4
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    //5
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) [webThumbnail loadRequest:request];
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
   [webThumbnail stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom = 15.0;"];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    NSString* javascript = [NSString stringWithFormat:@"window.scrollTo(0, 0);"];
    [webView stringByEvaluatingJavaScriptFromString:javascript];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setVideoInf:(VideoInf*) _videoInf
{
    myVideoInf = [[VideoInf alloc] init];
    myVideoInf = _videoInf;
}

- (IBAction)onPlay:(id)sender
{
//    NSURL *url = [[NSURL alloc] initWithString:myVideoInf.videoUrl];
    NSLog(@"%@", myVideoInf.videoUrl);
    NSURL *url=[[NSURL alloc] initWithString:myVideoInf.videoUrl];

    movieController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:movieController];
    
    movieController.controlStyle = MPMovieControlStyleDefault;
    movieController.shouldAutoplay = YES;
    
//    [self presentViewController:moviePlayer animated:YES completion:nil];
    [self.view addSubview:movieController.view];
    [movieController setFullscreen:YES animated:YES];
}

- (void)moviePlayBackDidFinish:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    [movieController stop];
    [movieController.view removeFromSuperview];
    
    movieController = nil;
}

@end
