//
//  VideoListViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoInf.h"

@interface VideoListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate>
{
    IBOutlet UITableView* myTableView;
    NSMutableArray* arrVideoInf;
    NSString* searchUrl;
    
    NSMutableData* receivedData;
    NSURLConnection* urlConnection;
    
    int nCurrentPage;
    int videoCount;
}

@property (nonatomic) int nListType;

- (void) setVideoArray:(NSMutableArray*) _arrVideoInf;
- (void) addPlayList: (VideoInf*) videoInf;
- (void) setUrl:(NSString*) url VIDEO_COUNT:(int) count;

@end
