//
//  SubmitViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/27/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "SubmitViewController.h"
#import "AccoutType.h"
#import "SelectionCell.h"
#import "UITableView+DataSourceBlocks.h"
#import "TableViewWithBlock.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface SubmitViewController ()

@end

@implementation SubmitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Membership";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [scrollView setContentSize:CGSizeMake(320, 800)];
    
    [self setAccountType];
    [self setCountry];
    [self setExperationMonth];
    [self setExperationYear];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setAccountType
{
    [cmbAccountType initTableViewDataSourceAndDelegate:^(UITableView *tableView,NSInteger section){
        return (NSInteger) [g_delegate.arrAccountType count];
        
    } setCellForIndexPathBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
        if (!cell) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SelectionCell" owner:self options:nil]objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        }
        
        AccoutType* accountType = [g_delegate.arrAccountType objectAtIndex:indexPath.row];
        [cell.lb setText:accountType.title];
        return cell;
    } setDidSelectRowBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=(SelectionCell*)[tableView cellForRowAtIndexPath:indexPath];
        txtAccountType.text=cell.lb.text;
        [btnDropDownAccountType sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    CGRect frame = cmbAccountType.frame;
    frame.size.height = 0;
    [cmbAccountType setFrame:frame];
    
    [cmbAccountType.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cmbAccountType.layer setBorderWidth:2];
}

- (void) setCountry
{
    isOpenCountry = NO;
    NSMutableArray* arrCountryName = [[NSMutableArray alloc] initWithObjects:
        @"United States", @"Canada", @"United Kingdom", @"Australia", @"Afghanistan", @"Albania", @"Algeria", @"American Samoa", @"Andorra", @"Angola", @"Anguilla", @"Antarctica", @"Antigua and Barbuda", @"Argentina", @"Armenia", @"Aruba", @"Austria", @"Azerbaijan", @"Bahamas", @"Bahrain", @"Bangladesh", @"Barbados", @"Belarus", @"Belgium", @"Belize", @"Benin", @"Bermuda", @"Bhutan", @"Bolivia", @"Bosnia and Herzegovina", @"Botswana", @"Brazil", @"Brunei Darussalam", @"Bulgaria", @"Burkina Faso", @"Burundi", @"Cambodia", @"Cameroon", @"Cape Verde", @"Cayman Islands", @"Central African Republic", @"Chad", @"Chile", @"China", @"Christmas Island", @"Cocos (Keeling) Islands", @"Colombia", @"Comoros", @"Congo", @"Congo, The Democratic Republic of the", @"Cook Islands", @"Costa Rica", @"Cote D`Ivoire", @"Croatia", @"Cyprus", @"Czech Republic", @"Denmark", @"Djibouti", @"Dominica", @"Dominican Republic", @"Ecuador", @"Egypt", @"El Salvador", @"Equatorial Guinea", @"Eritrea", @"Estonia", @"Ethiopia", @"Falkland Islands (Malvinas)", @"Faroe Islands", @"Fiji", @"Finland", @"France", @"French Guiana", @"French Polynesia", @"Gabon", @"Gambia", @"Georgia", @"Germany", @"Ghana", @"Gibraltar", @"Greece", @"Greenland", @"Grenada", @"Guadeloupe", @"Guam", @"Guatemala", @"Guinea", @"Guinea-Bissau", @"Guyana", @"Haiti", @"Honduras", @"Hong Kong", @"Hungary", @"Iceland", @"India", @"Indonesia", @"Iran (Islamic Republic Of)", @"Iraq", @"Ireland", @"Israel", @"Italy", @"Jamaica", @"Japan", @"Jordan", @"Kazakhstan", @"Kenya", @"Kiribati", @"Korea North", @"Korea South", @"Kuwait", @"Kyrgyzstan", @"Laos", @"Latvia", @"Lebanon", @"Lesotho", @"Liberia", @"Liechtenstein", @"Lithuania", @"Luxembourg", @"Macau", @"Macedonia", @"Madagascar", @"Malawi", @"Malaysia", @"Maldives", @"Mali", @"Malta", @"Marshall Islands", @"Martinique", @"Mauritania", @"Mauritius", @"Mexico", @"Micronesia", @"Moldova", @"Monaco", @"Mongolia", @"Montserrat", @"Morocco", @"Mozambique", @"Namibia", @"Nepal", @"Netherlands", @"Netherlands Antilles", @"New Caledonia", @"New Zealand", @"Nicaragua", @"Niger", @"Nigeria", @"Norway", @"Oman", @"Pakistan", @"Palau", @"Palestine Autonomous", @"Panama", @"Papua New Guinea", @"Paraguay", @"Peru", @"Philippines", @"Poland", @"Portugal", @"Puerto Rico", @"Qatar", @"Reunion", @"Romania", @"Russian Federation", @"Rwanda", @"Saint Vincent and the Grenadines", @"Saipan", @"San Marino", @"Saudi Arabia", @"Senegal", @"Seychelles", @"Sierra Leone", @"Singapore", @"Slovak Republic", @"Slovenia", @"Somalia", @"South Africa", @"Spain", @"Sri Lanka", @"St. Kitts/Nevis", @"St. Lucia", @"Sudan", @"Suriname", @"Swaziland", @"Sweden", @"Switzerland", @"Syria", @"Taiwan", @"Tajikistan", @"Tanzania", @"Thailand", @"Togo", @"Tokelau", @"Tonga", @"Trinidad and Tobago", @"Tunisia", @"Turkey", @"Turkmenistan", @"Turks and Caicos Islands", @"Tuvalu", @"Uganda", @"Ukraine", @"United Arab Emirates", @"Uruguay", @"Uzbekistan", @"Vanuatu", @"Venezuela", @"Viet Nam", @"Virgin Islands (British)", @"Virgin Islands (U.S.)", @"Wallis and Futuna Islands", @"Yemen", @"Yugoslavia", @"Zambia", @"Zimbabwe", nil];
    
    [cmbCountry initTableViewDataSourceAndDelegate:^(UITableView *tableView,NSInteger section){
        return (NSInteger) [arrCountryName count];
        
    } setCellForIndexPathBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
        if (!cell) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SelectionCell" owner:self options:nil]objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        }
        
        NSString* contryName = [arrCountryName objectAtIndex:indexPath.row];
        [cell.lb setText:contryName];
        return cell;
    } setDidSelectRowBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=(SelectionCell*)[tableView cellForRowAtIndexPath:indexPath];
        txtCountry.text=cell.lb.text;
        [btnDropDownCountry sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    CGRect frame = cmbCountry.frame;
    frame.size.height = 0;
    [cmbCountry setFrame:frame];
    
    [cmbCountry.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cmbCountry.layer setBorderWidth:2];
}

- (void) setExperationMonth
{
    isOpenExpirationMonth = NO;
    
    [cmbExpirationMonth initTableViewDataSourceAndDelegate:^(UITableView *tableView,NSInteger section){
        return (NSInteger) 12;
        
    } setCellForIndexPathBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
        if (!cell) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SelectionCell" owner:self options:nil]objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        }
        
        [cell.lb setText:[NSString stringWithFormat:@"%d", indexPath.row + 1]];
        return cell;
    } setDidSelectRowBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=(SelectionCell*)[tableView cellForRowAtIndexPath:indexPath];
        txtExpirationMonth.text=cell.lb.text;
        [btnDropDownExpirationMonth sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    CGRect frame = cmbExpirationMonth.frame;
    frame.size.height = 0;
    [cmbExpirationMonth setFrame:frame];
    
    [cmbExpirationMonth.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cmbExpirationMonth.layer setBorderWidth:2];
}

- (void) setExperationYear
{
    isOpenExpirationYear = NO;
    
    [cmbExpirationYear initTableViewDataSourceAndDelegate:^(UITableView *tableView,NSInteger section){
        return (NSInteger) 11;
        
    } setCellForIndexPathBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
        if (!cell) {
            cell=[[[NSBundle mainBundle]loadNibNamed:@"SelectionCell" owner:self options:nil]objectAtIndex:0];
            [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        }
        
        [cell.lb setText:[NSString stringWithFormat:@"%d", indexPath.row + 2014]];
        return cell;
    } setDidSelectRowBlock:^(UITableView *tableView,NSIndexPath *indexPath){
        SelectionCell *cell=(SelectionCell*)[tableView cellForRowAtIndexPath:indexPath];
        txtExpirationYear.text=cell.lb.text;
        [btnDropDownExpirationYear sendActionsForControlEvents:UIControlEventTouchUpInside];
    }];
    
    CGRect frame = cmbExpirationYear.frame;
    frame.size.height = 0;
    [cmbExpirationYear setFrame:frame];
    
    [cmbExpirationYear.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cmbExpirationYear.layer setBorderWidth:2];
}

- (IBAction)onChangeAccountType:(id)sender
{
    if (isOpenAccountType) {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *closeImage=[UIImage imageNamed:@"dropdown.png"];
            [btnDropDownAccountType setImage:closeImage forState:UIControlStateNormal];
            
            CGRect frame=cmbAccountType.frame;
            
            frame.size.height=0;
            [cmbAccountType setFrame:frame];
            
        } completion:^(BOOL finished){
            
            isOpenAccountType = NO;
        }];
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *openImage=[UIImage imageNamed:@"dropup.png"];
            [btnDropDownAccountType setImage:openImage forState:UIControlStateNormal];
            
            CGRect frame=cmbAccountType.frame;
            
            frame.size.height=200;
            [cmbAccountType setFrame:frame];
        } completion:^(BOOL finished){
            
            isOpenAccountType=YES;
        }];
    }
}

- (IBAction)onChangeCountry:(id)sender
{
    if (isOpenCountry) {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *closeImage=[UIImage imageNamed:@"dropdown.png"];
            [btnDropDownCountry setImage:closeImage forState:UIControlStateNormal];
            
            CGRect frame=cmbCountry.frame;
            
            frame.size.height=0;
            [cmbCountry setFrame:frame];
            
        } completion:^(BOOL finished){
            
            isOpenCountry = NO;
        }];
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *openImage=[UIImage imageNamed:@"dropup.png"];
            [btnDropDownCountry setImage:openImage forState:UIControlStateNormal];
            
            CGRect frame=cmbCountry.frame;
            
            frame.size.height=200;
            [cmbCountry setFrame:frame];
        } completion:^(BOOL finished){
            
            isOpenCountry=YES;
        }];
    }
}

- (IBAction)onChangeExpirationMonth:(id)sender
{
    if (isOpenExpirationMonth) {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *closeImage=[UIImage imageNamed:@"dropdown.png"];
            [btnDropDownExpirationMonth setImage:closeImage forState:UIControlStateNormal];
            
            CGRect frame=cmbExpirationMonth.frame;
            
            frame.size.height=0;
            [cmbExpirationMonth setFrame:frame];
            
        } completion:^(BOOL finished){
            
            isOpenExpirationMonth = NO;
        }];
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *openImage=[UIImage imageNamed:@"dropup.png"];
            [btnDropDownExpirationMonth setImage:openImage forState:UIControlStateNormal];
            
            CGRect frame=cmbExpirationMonth.frame;
            
            frame.size.height=200;
            [cmbExpirationMonth setFrame:frame];
        } completion:^(BOOL finished){
            
            isOpenExpirationMonth=YES;
        }];
    }
}

- (IBAction)onChangeExpirationYear:(id)sender
{
    if (isOpenExpirationYear) {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *closeImage=[UIImage imageNamed:@"dropdown.png"];
            [btnDropDownExpirationYear setImage:closeImage forState:UIControlStateNormal];
            
            CGRect frame=cmbExpirationYear.frame;
            
            frame.size.height=0;
            [cmbExpirationYear setFrame:frame];
            
        } completion:^(BOOL finished){
            
            isOpenExpirationYear = NO;
        }];
    } else {
        
        [UIView animateWithDuration:0.3 animations:^{
            UIImage *openImage=[UIImage imageNamed:@"dropup.png"];
            [btnDropDownExpirationYear setImage:openImage forState:UIControlStateNormal];
            
            CGRect frame=cmbExpirationYear.frame;
            
            frame.size.height=200;
            [cmbExpirationYear setFrame:frame];
        } completion:^(BOOL finished){
            
            isOpenExpirationYear=YES;
        }];
    }
}

@end
