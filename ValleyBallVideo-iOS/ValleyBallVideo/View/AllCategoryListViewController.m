//
//  AllCategoryListViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "AllCategoryListViewController.h"
#import "CategoryTableViewCell.h"
#import "VideoCategory.h"
#import "VideoListViewController.h"
#import "SearchCategory.h"
#import "SearchItem.h"
#import "Global.h"
#import "AppDelegate.h"

@interface AllCategoryListViewController ()

@end

@implementation AllCategoryListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"All Videos";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate showWaitingScreen:@"Please wait.." bShowText:YES];
    
    UINib *niCategoryCell = [UINib nibWithNibName:@"CategoryTableViewCell" bundle:nil];
    [myTableView registerNib: niCategoryCell forCellReuseIdentifier:@"CategoryTableViewCell"];
    
    [self makeCategoryArray];
    
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
    NSMutableString *post_data     = [[NSMutableString alloc] init];
    receivedData = [NSMutableData dataWithCapacity: 0];
    
    [urlRequest setURL:[NSURL URLWithString:GET_VIDEO_COUNT_PATH]];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"GET"];
    [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
    urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
}

- (void) makeCategoryArray
{
    arrCategory = [[NSMutableArray alloc] init];
    VideoCategory* category = nil;
    VideoInf* videoInf = nil;

    category = [[VideoCategory alloc] initWithName:@"SETTING DRILLS"];
    category.searchKey = @"setting-drills";
    [arrCategory addObject:category];
    
//    videoInf = [[VideoInf alloc] init];
//    videoInf.videoUrl = @"http://www.youtube.com/watch?feature=player_embedded&v=prSfG7gN7Js";
//    videoInf.postImageUrl = @"http://www.theartofcoachingvolleyball.com/wp-content/uploads/2013/09/Screen-Shot-2013-09-09-at-10.16.26-PM-230x130.png";
//    videoInf.title = @"Setting for Beginners – At Home Drills";
//    videoInf.desc = @"The Basics of Setting taught by Oregon State Volleyball Assistant Coach Emily Hiza. This video covers basic hand positioning, leg use, and the follow through. It also demonstrates a few drills young volleyball players can do at home.\n\nLeave a comment below on issues you have had with teaching younger players the basic skills of volleyball and we’ll try to put together a video for you.";
//    
//    [category.arrVideo addObject: videoInf];
//    
//    videoInf = [[VideoInf alloc] init];
//    videoInf.videoUrl = @"http://fast.wistia.net/embed/iframe/jgf9uxr6qb";
//    videoInf.postImageUrl = @"http://www.theartofcoachingvolleyball.com/wp-content/uploads/2013/05/Setter-Awarness-John-Dunning1-230x130.jpg";
//    videoInf.title = @"Setter Awareness Drill with John Dunning";
//    videoInf.desc = @"The Basics of Setting taught by Oregon State Volleyball Assistant Coach Emily Hiza. This video covers basic hand positioning, leg use, and the follow through. It also demonstrates a few drills young volleyball players can do at home.\n\nLeave a comment below on issues you have had with teaching younger players the basic skills of volleyball and we’ll try to put together a video for you.";
//    
//    [category.arrVideo addObject: videoInf];

    
    category = [[VideoCategory alloc] initWithName:@"SERVING DRILLS"];
    category.searchKey = @"serving-drills";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"BLOCKING DRILLS"];
    category.searchKey = @"blocking-drills";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"HITTING DRILLS"];
    category.searchKey = @"hitting-drills";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"PASSING DRILLS"];
    category.searchKey = @"passing-drills";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"DEFENCE DRILLS"];
    category.searchKey = @"individual-defense-drills";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"BALL CONTROL DRILLS"];
    category.searchKey = @"ball-control-drills";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"WARM UP DRILLS"];
    category.searchKey = @"warm-up-2";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"TEAM DRILLS"];
    category.searchKey = @"team-drills";
    [arrCategory addObject:category];
    category = [[VideoCategory alloc] initWithName:@"DRILL DIAGRAMS"];
    category.searchKey = @"drill-diagram";
    [arrCategory addObject:category];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSLog(@"category count = %d", arrCategory.count);
    return [arrCategory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *categoryCellIdentifier = @"CategoryTableViewCell";

    CategoryTableViewCell *categoryCell = (CategoryTableViewCell*) [tableView dequeueReusableCellWithIdentifier: categoryCellIdentifier forIndexPath:indexPath];
    VideoCategory* category = [arrCategory objectAtIndex:indexPath.row];
    
    [categoryCell setCategory:category];
    
    return categoryCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideoListViewController* videoListViewCtrl = nil;
    VideoCategory* category = [arrCategory objectAtIndex:indexPath.row];

    videoListViewCtrl = [[VideoListViewController alloc] initWithNibName:@"VideoListViewController" bundle:nil];
    
    [self.navigationController pushViewController:videoListViewCtrl animated:YES];
    
//    [videoListViewCtrl setVideoArray:category.arrVideo];
    [videoListViewCtrl setTitle:category.name];
    videoListViewCtrl.nListType = CATEGORY_LIST;

    NSString* url = [NSString stringWithFormat:@"%@?category=%@", ALL_VIDEO_FILE_PATH, category.searchKey];
    [videoListViewCtrl setUrl:url VIDEO_COUNT:category.countVideo];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnection) {
        [receivedData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnection) {
        NSString *myString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        NSLog(@"login result = %@", myString);
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        NSArray* arrJsonSearchCount = [jsonObject objectForKey:@"message"];
        
        if ([arrJsonSearchCount isKindOfClass:[NSString class]]) {
            
        } else {
            
            int k = 0;
            for (VideoCategory* category in arrCategory) {
                id jsonSearchCount = [arrJsonSearchCount objectAtIndex:k];
                int count = [[jsonSearchCount objectForKey:category.searchKey] intValue];
//                int k = [[arrJsonSearchCount objectForKey:category.searchKey] intValue];
                category.countVideo = count;
                k++;
            }
//            NSLog(@"video count = %d", [arrJsonSearchCount count]);
        }
        
        [myTableView reloadData];
        AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        [delegate hideWaitingScreen];
    }
    // do the same for cards connection
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // Release the connection and the data object
    // by setting the properties (declared elsewhere)
    // to nil.  Note that a real-world app usually
    // requires the delegate to manage more than one
    // connection at a time, so these lines would
    // typically be replaced by code to iterate through
    // whatever data structures you are using.
    urlConnection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

@end
