//
//  RegisterViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "RegisterViewController.h"
#import "SubmitViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Register";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) onSilver:(id)sender
{
    SubmitViewController* submitViewCtrl = [[SubmitViewController alloc] initWithNibName:@"SubmitViewController" bundle:nil];
    [self.navigationController pushViewController:submitViewCtrl animated:YES];
}

- (IBAction) onGlod:(id)sender
{
    SubmitViewController* submitViewCtrl = [[SubmitViewController alloc] initWithNibName:@"SubmitViewController" bundle:nil];
    [self.navigationController pushViewController:submitViewCtrl animated:YES];
}

- (IBAction) onPlatnium:(id)sender
{
    SubmitViewController* submitViewCtrl = [[SubmitViewController alloc] initWithNibName:@"SubmitViewController" bundle:nil];
    [self.navigationController pushViewController:submitViewCtrl animated:YES];
}

- (IBAction) onCoach:(id)sender
{
    SubmitViewController* submitViewCtrl = [[SubmitViewController alloc] initWithNibName:@"SubmitViewController" bundle:nil];
    [self.navigationController pushViewController:submitViewCtrl animated:YES];
}

- (IBAction) onCoachPlus:(id)sender
{
    SubmitViewController* submitViewCtrl = [[SubmitViewController alloc] initWithNibName:@"SubmitViewController" bundle:nil];
    [self.navigationController pushViewController:submitViewCtrl animated:YES];
}

@end
