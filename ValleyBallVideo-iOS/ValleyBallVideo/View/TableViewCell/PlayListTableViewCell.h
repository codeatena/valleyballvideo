//
//  PlayListTableViewCell.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListInf.h"

@interface PlayListTableViewCell : UITableViewCell
{
    IBOutlet UILabel*    lblTitle;
    IBOutlet UILabel*    lblCount;
    
    PlayListInf* myPlayListInf;
}

- (void) setPlayListInf: (PlayListInf*) playList;

@end
