//
//  CategoryTableViewCell.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoCategory.h"

@interface CategoryTableViewCell : UITableViewCell
{
    IBOutlet UILabel* lblCategoryName;
    IBOutlet UILabel* lblVideoCount;
}

- (void) setCategory: (VideoCategory*) category;

@end
