//
//  ChoosePlayListTableViewCell.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ChoosePlayListTableViewCell.h"

@implementation ChoosePlayList_TableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setPlayListInf:(PlayListInf*) playListInf
{
    myPlayListInf = [[PlayListInf alloc] init];
    myPlayListInf = playListInf;
    
    [lblTitle setText:myPlayListInf.title];
    [lblCount setText:[NSString stringWithFormat:@"%d", myPlayListInf.count]];
    
    if (playListInf.isSelected) {
        imgChooseStatus.image = [UIImage imageNamed:@"checked.png"];
    } else {
        imgChooseStatus.image = [UIImage imageNamed:@"unchecked.png"];
    }
}

@end
