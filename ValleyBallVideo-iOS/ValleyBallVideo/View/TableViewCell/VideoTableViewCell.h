//
//  VideoTableViewCell.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/15/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoInf.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoTableViewCell : UITableViewCell
{
    IBOutlet    UIImageView*    imgThumbnail;
    IBOutlet    UIImageView*    imgPremium;

    IBOutlet    UILabel*        lblTitle;
    IBOutlet    UIButton*       btnPlay;
    IBOutlet    UIWebView*      webThumbnail;
    IBOutlet    UIButton*       btnAddPlaylist;
    
    VideoInf*                   myVideoInf;
    
    MPMoviePlayerController* movieController;    
}

@property(nonatomic, strong) UIViewController* parentViewCtrl;

- (void) setVideoInf:(VideoInf*) videoInf;
- (void) setVideoInfOnlyTitle:(VideoInf *) videoInf;
- (void) setVideoInfOnlyImage:(VideoInf *) videoInf;

- (IBAction) onPlay:(id)sender;
- (IBAction) onAddPlayList:(id)sender;

@end