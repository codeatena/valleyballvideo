//
//  VideoTableViewCell.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/15/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "VideoTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "VideoListViewController.h"
#import "FeaturedListViewController.h"
#import "AppDelegate.h"
#import "Global.h"

@implementation VideoTableViewCell

@synthesize parentViewCtrl;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setVideoInf:(VideoInf*) videoInf
{
    myVideoInf = [[VideoInf alloc] init];
    myVideoInf = videoInf;
    
    [lblTitle setText:videoInf.title];
    [btnPlay setHidden:YES];
    
    [btnAddPlaylist setHidden:NO];
    imgThumbnail.hidden = NO;
    imgPremium.hidden = NO;
    lblTitle.hidden = NO;
    
    [lblTitle setFrame:CGRectMake(107, 3, 180, 74)];
    [imgThumbnail setFrame:CGRectMake(0, 0, 99, 79)];
    webThumbnail.hidden = YES;

    imgPremium.hidden = !videoInf.isPremium;
    
    if (g_delegate.curUser != nil && videoInf.isPremium) {   /// before login
        int nLevel = g_delegate.curUser.nLevel;
        
        if (nLevel == 2)    imgPremium.hidden = videoInf.isFree;
        if (nLevel == 13)   imgPremium.hidden = videoInf.isPlatinum;
        if (nLevel == 5)   imgPremium.hidden = videoInf.isGold;
        if (nLevel == 4)   imgPremium.hidden = videoInf.isSilver;
        if (nLevel == 18)   imgPremium.hidden = videoInf.isSmallClub;
        if (nLevel == 19)   imgPremium.hidden = videoInf.isLargeClub;
    }

    if (g_delegate.curUser == nil) {
        [btnAddPlaylist setHidden:YES];
    } else {
        [btnAddPlaylist setHidden:NO];
    }
    
    if ([parentViewCtrl isKindOfClass: [VideoListViewController class]]) {
        VideoListViewController* videoListViewCtrl = (VideoListViewController*) parentViewCtrl;
        if (videoListViewCtrl.nListType == MY_PLAY_LIST) {
            [btnAddPlaylist setHidden:YES];
        }
    }
    
    NSString* postImageUrl = videoInf.postImageUrl;
    
    [imgThumbnail setImageWithURL:[NSURL URLWithString:postImageUrl]
                 placeholderImage:[UIImage imageNamed:postImageUrl]
      usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
}

- (void) setVideoInfOnlyTitle:(VideoInf *) videoInf
{
    myVideoInf = [[VideoInf alloc] init];
    myVideoInf = videoInf;
    btnAddPlaylist.hidden = YES;
    
    [imgThumbnail setHidden:NO];
    imgPremium.hidden = YES;
    [btnPlay setHidden:YES];
    CGRect rect = lblTitle.frame;
    NSLog(@"x=%f, y=%f, w=%f, h=%f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    [lblTitle setFrame:CGRectMake(3, rect.origin.y, 280, rect.size.height)];
    [lblTitle setText:videoInf.title];
}

- (void) setVideoInfOnlyImage:(VideoInf *) videoInf
{
    myVideoInf = [[VideoInf alloc] init];
    myVideoInf = videoInf;
    
    [lblTitle setHidden:YES];
    [imgThumbnail setFrame:CGRectMake(0, 0, 320, 79)];
    [imgThumbnail setHidden:YES];
    [btnAddPlaylist setHidden:YES];
    [btnPlay setHidden:YES];

    imgPremium.hidden = YES;
    [webThumbnail setHidden:NO];
//    [btnPlay setHidden:YES];
    
//    btnPlay.center = CGPointMake(160, 50);
    NSString* postImageUrl = videoInf.postImageUrl;
    [imgThumbnail setImageWithURL:[NSURL URLWithString:postImageUrl]
                 placeholderImage:[UIImage imageNamed:postImageUrl]
      usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    NSString *urlString = myVideoInf.videoUrl;

    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) [webThumbnail loadRequest:request];
         else if (error != nil) NSLog(@"Error: %@", error);
     }];
    
    self.accessoryType = UITableViewCellAccessoryNone;
}

- (IBAction) onPlay:(id)sender
{
    NSLog(@"%@", myVideoInf.videoUrl);
    NSURL *url=[[NSURL alloc] initWithString:myVideoInf.videoUrl];
    
    movieController = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:movieController];
    
    movieController.controlStyle = MPMovieControlStyleDefault;
    movieController.shouldAutoplay = YES;
    
    //    [self presentViewController:moviePlayer animated:YES completion:nil];
    [self.contentView addSubview:movieController.view];
    [movieController setFullscreen:YES animated:YES];
}

- (IBAction) onAddPlayList:(id)sender
{
    if ([parentViewCtrl isKindOfClass: [VideoListViewController class]]) {
        VideoListViewController* videoListViewCtrl = (VideoListViewController*) parentViewCtrl;
        [videoListViewCtrl addPlayList:myVideoInf];
    }
    if ([parentViewCtrl isKindOfClass: [FeaturedListViewController class]]) {
        FeaturedListViewController* featuredListViewCtrl = (FeaturedListViewController*) parentViewCtrl;
        [featuredListViewCtrl addPlayList:myVideoInf];
    }
}

@end
