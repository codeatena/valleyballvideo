//
//  PlayListTableViewCell.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/18/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "PlayListTableViewCell.h"

@implementation PlayListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setPlayListInf: (PlayListInf*) playList
{
    myPlayListInf = [[PlayListInf alloc] init];
    myPlayListInf = playList;
    
    [lblTitle setText:myPlayListInf.title];
    [lblCount setText:[NSString stringWithFormat:@"%d", myPlayListInf.count]];
}

@end
