//
//  ChoosePlayListTableViewCell.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayListInf.h"

@interface ChoosePlayList_TableViewCell : UITableViewCell
{
    IBOutlet UIImageView*       imgChooseStatus;
    IBOutlet UILabel*           lblTitle;
    IBOutlet UILabel*           lblCount;
    
    PlayListInf* myPlayListInf;
}

- (void) setPlayListInf:(PlayListInf*) playListInf;

@end
