//
//  VideoPlayerViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/17/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoInf.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoPlayerViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIImageView*       imgThumbnail;
    IBOutlet UILabel*           lblTitle;
    IBOutlet UILabel*           lblDesc;
    IBOutlet UILabel*           lblDate;
    
    IBOutlet UIScrollView*      scrollView;
    
    IBOutlet UIWebView*         webThumbnail;
    
    VideoInf* myVideoInf;
    
    MPMoviePlayerController *movieController;
    
    BOOL isFullScreen;
}

- (void) setVideoInf:(VideoInf*) _videoInf;

- (IBAction)onPlay:(id)sender;

@end
