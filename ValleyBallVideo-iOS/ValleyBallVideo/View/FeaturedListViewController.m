//
//  FeaturedListViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/13/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "FeaturedListViewController.h"
#import "CategoryTableViewCell.h"
#import "VideoPlayerViewController.h"
#import "VideoTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "ChoosePlayListViewController.h"
#import "AppDelegate.h"
#import "Global.h"

@interface FeaturedListViewController ()

@end

@implementation FeaturedListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Featured Videos";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UINib *nibVideoCell = [UINib nibWithNibName:@"VideoTableViewCell" bundle:nil];
    [myTableView registerNib: nibVideoCell forCellReuseIdentifier:@"VideoTableViewCell"];
    
//    [self makeFeaturedList];
//    [self setLatestFeature];
    
    [self getFeatureList];
}

- (void) viewWillAppear:(BOOL)animated
{
    [myTableView reloadData];
}

- (void) getFeatureList
{
    NSString* url = [NSString stringWithFormat:@"%@", GET_FEATURED_PATH];
    
    NSLog(@"get featured url = %@", url);
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
    NSMutableString *post_data     = [[NSMutableString alloc] init];
    receivedData = [NSMutableData dataWithCapacity: 0];
    
    [urlRequest setURL:[NSURL URLWithString:url]];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"GET"];
    [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
    urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [g_delegate showWaitingScreen:@"Please wait.." bShowText:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSLog(@"Video count = %d", arrFeatured.count);
    if (arrFeatured.count == 0) {
        return 0;
    }
    return [arrFeatured count] + 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 180;
    
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *videoCellIdentifier = @"VideoTableViewCell";
    
    VideoTableViewCell *videoCell = (VideoTableViewCell*) [tableView dequeueReusableCellWithIdentifier: videoCellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        [videoCell setVideoInfOnlyImage:latestFeatureVideoInf];

    } else if (indexPath.row == 1) {
        [videoCell setVideoInfOnlyTitle:latestFeatureVideoInf];
    } else {
        VideoInf* videoInf = [arrFeatured objectAtIndex:indexPath.row - 2];
        [videoCell setVideoInf:videoInf];
    }

    videoCell.parentViewCtrl = self;
    
    return videoCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideoPlayerViewController* videoPlayerViewCtrl = nil;
    videoPlayerViewCtrl = [[VideoPlayerViewController alloc] initWithNibName:@"VideoPlayerViewController" bundle:nil];
    
    if (indexPath.row == 0) {
        
    } else if (indexPath.row == 1) {
        [self.navigationController pushViewController:videoPlayerViewCtrl animated:YES];
        [videoPlayerViewCtrl setVideoInf:latestFeatureVideoInf];
    } else {
        VideoInf* videoInf = [arrFeatured objectAtIndex:indexPath.row - 2];

        BOOL isCanPlay = YES;
        
        if (videoInf.isPremium) {
            isCanPlay = NO;
            if (g_delegate.curUser != nil) {
                isCanPlay = NO;
                int nLevel = g_delegate.curUser.nLevel;
                if (nLevel == 2 && videoInf.isFree) {
                    isCanPlay = YES;
                }
                if (nLevel == 13 && videoInf.isPlatinum) {
                    isCanPlay = YES;
                }
                if (nLevel == 5 && videoInf.isGold) {
                    isCanPlay = YES;
                }
                if (nLevel == 4 && videoInf.isSilver) {
                    isCanPlay = YES;
                }
                if (nLevel == 18 && videoInf.isSmallClub) {
                    isCanPlay = YES;
                }
                if (nLevel == 19 && videoInf.isLargeClub) {
                    isCanPlay = YES;
                }
            }
        }
        
        if (isCanPlay) {
            
            videoPlayerViewCtrl = [[VideoPlayerViewController alloc] initWithNibName:@"VideoPlayerViewController" bundle:nil];
            [self.navigationController pushViewController:videoPlayerViewCtrl animated:YES];
            
            [videoPlayerViewCtrl setVideoInf:videoInf];
            [tableView deselectRowAtIndexPath: indexPath animated: YES];
        } else {
            [self showLoginAlert];
        }
    }

    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void) showLoginAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                    message:@"Your account is not enough to see this video."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) makeFeaturedList
{
//    arrFeatured = [[NSMutableArray alloc] init];
//    VideoInf* videoInf = nil;
//    
//    videoInf = [[VideoInf alloc] init];
//    videoInf.videoUrl = @"http://www.youtube.com/watch?feature=player_embedded&v=prSfG7gN7Js";
//    videoInf.postImageUrl = @"http://www.theartofcoachingvolleyball.com/wp-content/uploads/2013/09/Screen-Shot-2013-09-09-at-10.16.26-PM-230x130.png";
//    videoInf.title = @"Setting for Beginners – At Home Drills";
//    videoInf.desc = @"The Basics of Setting taught by Oregon State Volleyball Assistant Coach Emily Hiza. This video covers basic hand positioning, leg use, and the follow through. It also demonstrates a few drills young volleyball players can do at home.\n\nLeave a comment below on issues you have had with teaching younger players the basic skills of volleyball and we’ll try to put together a video for you.";
//    
//    [arrFeatured addObject:videoInf];
//    
//    videoInf = [[VideoInf alloc] init];
//    videoInf.videoUrl = @"http://fast.wistia.net/embed/iframe/jgf9uxr6qb";
//    videoInf.postImageUrl = @"http://www.theartofcoachingvolleyball.com/wp-content/uploads/2013/05/Setter-Awarness-John-Dunning1-230x130.jpg";
//    videoInf.title = @"Setter Awareness Drill with John Dunning";
//    videoInf.desc = @"The Basics of Setting taught by Oregon State Volleyball Assistant Coach Emily Hiza. This video covers basic hand positioning, leg use, and the follow through. It also demonstrates a few drills young volleyball players can do at home.\n\nLeave a comment below on issues you have had with teaching younger players the basic skills of volleyball and we’ll try to put together a video for you.";
//    
//    [arrFeatured addObject:videoInf];
}

- (void) setLatestFeature
{
    latestFeatureVideoInf = [arrFeatured objectAtIndex:0];
//    NSString* postImageUrl = latestFeatureVideoInf.postImageUrl;
    
//    [imgLatestFeatureVideo setImageWithURL:[NSURL URLWithString:imageUrl]
//                 placeholderImage:[UIImage imageNamed:imageUrl]
//      usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
}

- (void) addPlayList: (VideoInf*) videoInf
{
    ChoosePlayListViewController* choosePlayListViewCtrl = [[ChoosePlayListViewController alloc] initWithNibName:@"ChoosePlayListViewController" bundle:nil];
    
    choosePlayListViewCtrl.myVideoInf = videoInf;
    UINavigationController* navCtrl = [[UINavigationController alloc] initWithRootViewController:choosePlayListViewCtrl];
    [self.navigationController presentViewController:navCtrl animated:YES completion:nil];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnection) {
        [receivedData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnection) {
        NSString *myString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        NSLog(@"featured result = %@", myString);
        NSError *error = nil;
        id jsonObject = [NSJSONSerialization
                         JSONObjectWithData:receivedData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        NSArray* arrJsonVideo = [jsonObject objectForKey:@"message"];
        
        if ([arrJsonVideo isKindOfClass:[NSString class]]) {
            
        } else {
            arrFeatured = [[NSMutableArray alloc] init];
            int k = 0;
            for (NSDictionary* jsonVideo in arrJsonVideo) {
                
                VideoInf* video = [[VideoInf alloc] init];
                video.ID = [jsonVideo objectForKey:@"ID"];
                video.title = [jsonVideo objectForKey:@"post_title"];
                video.desc = [jsonVideo valueForKey:@"post_content"];
                if (video.desc == nil) video.desc = @"";
                video.videoImageUrl = [jsonVideo objectForKey:@"video_thumbnail"];
                video.postImageUrl = [jsonVideo valueForKey:@"post_thumbnail"];
                video.videoUrl = [jsonVideo objectForKey:@"video_url"];
                video.postDate = [jsonVideo objectForKey:@"post_date"];
                
                if ([video.postImageUrl isKindOfClass:[NSNull class]]) {
                    NSLog(@"nil");
                    video.postImageUrl = @"";
                }
                
                video.isFree = (BOOL) [jsonVideo valueForKey:@"free"];
                video.isPlatinum = (BOOL) [jsonVideo valueForKey:@"Platinum"];
                video.isSilver = (BOOL) [jsonVideo valueForKey:@"Silver_Member"];
                video.isGold = (BOOL) [jsonVideo valueForKey:@"Gold_Member"];
                video.isSmallClub = (BOOL) [jsonVideo valueForKey:@"small_club"];
                video.isLargeClub = (BOOL) [jsonVideo valueForKey:@"large_club"];
                
                if (video.isFree || video.isPlatinum || video.isSilver || video.isGold || video.isSmallClub || video.isLargeClub) {
                    video.isPremium = YES;
                }
                
//                NSLog(@"video %@ free value: %d", video.ID, video.isPlatinum);
                
//                NSLog(@"post title %d = %@", k, video.de);
//                NSLog(@"postImageUrl %d = %@", k, video.postImageUrl);
                
                [arrFeatured addObject:video];
                k++;
            }
            [self setLatestFeature];
            //            NSLog(@"video count = %d", [arrJsonVideo count]);
        }

        [g_delegate hideWaitingScreen];
        [myTableView reloadData];
    }
    // do the same for cards connection
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // Release the connection and the data object
    // by setting the properties (declared elsewhere)
    // to nil.  Note that a real-world app usually
    // requires the delegate to manage more than one
    // connection at a time, so these lines would
    // typically be replaced by code to iterate through
    // whatever data structures you are using.
    urlConnection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

@end