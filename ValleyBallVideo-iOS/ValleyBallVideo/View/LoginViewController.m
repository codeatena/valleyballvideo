//
//  LoginViewController.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "AppDelegate.h"
#import "MainTabBarViewController.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "Global.h"
#import "MyPlayListViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Login";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onRegister:(id)sender
{
    RegisterViewController* registerViewCtrl = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:registerViewCtrl animated:YES];
}

- (IBAction)onLogin:(id)sender
{
    //    [self loginSuccess];
    [self login];
}

- (void) login
{
    NSString* username = txtUserName.text;
    NSString* password = txtPassword.text;
    
    [txtUserName resignFirstResponder];
    [txtPassword resignFirstResponder];
    
    NSMutableURLRequest* urlRequest = [[NSMutableURLRequest alloc] init];
    NSMutableString *post_data     = [[NSMutableString alloc] init];
    [post_data appendFormat:@"%@", [NSString stringWithFormat:@"username=%@&password=%@", username, password]];
    
    [urlRequest setURL:[NSURL URLWithString:LOGIN_FILE_PATH]];
    [urlRequest addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[post_data dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    NSMutableData *data_users = [[NSMutableData alloc] init]; // add this line in your code
    urlConnection   = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [delegate showWaitingScreen:@"Please wait.." bShowText:YES];
}

- (IBAction)onDone:(id)sender
{
//    [self login];

    UITextField* txtField = (UITextField*) sender;
    [txtField resignFirstResponder];
}

- (void) loginSuccess
{
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    NSMutableArray* arrTabViewControllers = [[NSMutableArray alloc] initWithArray:delegate.mainTabBarCtrl.viewControllers];
    
    [arrTabViewControllers removeObjectAtIndex:2];
    
    MyPlayListViewController* myPlayListViewCtrl = nil;
    myPlayListViewCtrl = [[MyPlayListViewController alloc] initWithNibName:@"MyPlayListViewController" bundle:nil];

    UINavigationController* navMyPlayList = [[UINavigationController alloc] initWithRootViewController:myPlayListViewCtrl];

    [arrTabViewControllers insertObject:navMyPlayList atIndex:2];
    
    [delegate.mainTabBarCtrl setViewControllers:arrTabViewControllers];
    
    UITabBar *tabBar = delegate.mainTabBarCtrl.tabBar;
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    
    tabBarItem3.image = [UIImage imageNamed:@"tab_my_playlist.png"];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == urlConnection) {
        
        AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        [delegate hideWaitingScreen];

        NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"login result = %@", myString);
        
        NSError *error = nil;
        
        id jsonObject = [NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:NSJSONReadingAllowFragments
                                   error:&error];
        if (jsonObject != nil && error == nil){
            NSLog(@"Successfully deserialized...");
            
            //check if the country code was valid
            NSDictionary *message = [jsonObject objectForKey:@"message"];
            NSString* strMessage = nil;
            
            if ([message isKindOfClass:[NSString class]]) {
                strMessage = (NSString*) message;
                NSLog(@"message = %@", message);
            } else {
                strMessage = [message valueForKey:@"message"];
                NSLog(@"message = %@", strMessage);
            }

            if ([strMessage isEqualToString:@"Logged in Successfully"]) {
                NSDictionary* data = [message objectForKey:@"data"];
                NSString* user_id = [data valueForKey:@"userid"];
                NSLog(@"user_id = %@", user_id);
                
                g_delegate.curUser = [[User alloc] init];
                g_delegate.curUser.userID = user_id;
                g_delegate.curUser.nLevel = [[data valueForKey:@"membership_level"] intValue];

                NSLog(@"membership_level = %d", g_delegate.curUser.nLevel);
                [self loginSuccess];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:strMessage
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (connection == urlConnection) {
        // use data from data_users

    }
    // do the same for cards connection
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    // Release the connection and the data object
    // by setting the properties (declared elsewhere)
    // to nil.  Note that a real-world app usually
    // requires the delegate to manage more than one
    // connection at a time, so these lines would
    // typically be replaced by code to iterate through
    // whatever data structures you are using.
    urlConnection = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

@end
