//
//  AllCategoryListViewController.h
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/14/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllCategoryListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate>
{
    IBOutlet UITableView* myTableView;
    NSMutableArray* arrCategory;
    
    NSMutableData* receivedData;
    NSURLConnection* urlConnection;
}

@end