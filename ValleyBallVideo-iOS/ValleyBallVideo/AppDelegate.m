//
//  AppDelegate.m
//  ValleyBallVideo
//
//  Created by JinSung Han on 4/13/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "AppDelegate.h"
#import "MainTabBarViewController.h"
#import "Common.h"
#import "AccoutType.h"

extern AppDelegate* g_delegate;

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize mainTabBarCtrl, curUser, arrMyPlayList, arrAccountType;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [NSThread sleepForTimeInterval:3.0];
    g_delegate = self;
    
    curUser = nil;
    mainTabBarCtrl = [[MainTabBarViewController alloc] init];
    [self.window setRootViewController: mainTabBarCtrl];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

- (void) setAccountType
{
    arrAccountType = [[NSMutableArray alloc] init];
    AccoutType* accountType = nil;
    
    accountType = [[AccoutType alloc] initWithTitle:@"Free"];
    accountType.jsonKey = @"free";
    accountType.nLevel = 2;
    [arrAccountType addObject:accountType];
    
    accountType = [[AccoutType alloc] initWithTitle:@"Silver"];
    accountType.jsonKey = @"Silver_Member";
    accountType.nLevel = 17;
    [arrAccountType addObject:accountType];
    
    accountType = [[AccoutType alloc] initWithTitle:@"Gold"];
    accountType.jsonKey = @"Gold_Member";
    accountType.nLevel = 16;
    [arrAccountType addObject:accountType];
    
    accountType = [[AccoutType alloc] initWithTitle:@"Platnium"];
    accountType.jsonKey = @"Platinum";
    accountType.nLevel = 15;
    [arrAccountType addObject:accountType];
    
    accountType = [[AccoutType alloc] initWithTitle:@"Coach"];
    accountType.jsonKey = @"small_club";
    accountType.nLevel = 18;
    [arrAccountType addObject:accountType];
    
    accountType = [[AccoutType alloc] initWithTitle:@"Coach+"];
    accountType.jsonKey = @"large_club";
    accountType.nLevel = 19;
    [arrAccountType addObject:accountType];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ValleyBallVideo" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ValleyBallVideo.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)showWaitingScreen:(NSString *)strText  bShowText:(BOOL)bShowText {
    
    UIView *view = [[UIView alloc] init];
    
    if ([Common is_Phone]) {
        if ([Common is_Retina]) {
            if ([Common getDeviceType] == DEVICE_TYPE_IPHONE_RETINA_35_INCH) {
                [view setFrame:CGRectMake(0, 0, 320, 480)];
            }
            else {
                [view setFrame:CGRectMake(0, 0, 320, 568)];
            }
        }
        else {
            [view setFrame:CGRectMake(0, 0, 320, 480)];
        }
    }
    else {
        if ([Common is_Retina]) {
            [view setFrame:CGRectMake(0, 0, 768, 1024)];
        }
        else {
            [view setFrame:CGRectMake(0, 0, 768, 1024)];
        }
    }
    
    [view setTag:TAG_WAIT_SCREEN_VIEW];
    [view setBackgroundColor:[UIColor clearColor]];
    [view setAlpha:1.0f];
    
    if (bShowText) {
        UIView *subView = [[UIView alloc] init];
        [subView setBackgroundColor:[UIColor blackColor]];
        [subView setAlpha:0.6];
        
        int width = 0;
        int height = 0;
        
        if ([Common is_Phone]) {
            width = 150;
            height = 100;
        }
        else {
            width = 300;
            height = 200;
        }
        
        [subView setFrame:CGRectMake(view.frame.size.width/2-width/2, view.frame.size.height/2-height/2, width, height)];
        
        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [indicatorView setTag:TAG_WAIT_SCREEN_INDICATOR];
        
        CGRect rectIndicatorViewFrame = [indicatorView frame];
        
        width = rectIndicatorViewFrame.size.width;
        height = rectIndicatorViewFrame.size.height;
        
        [indicatorView setFrame:CGRectMake(subView.frame.size.width/2-width/2, subView.frame.size.height/3-width/2, width, height)];
        
        [indicatorView startAnimating];
        [subView addSubview:indicatorView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, subView.frame.size.height * 2/3, subView.frame.size.width, subView.frame.size.height/3)];
        [label setText:strText];
        
        [label setBackgroundColor:[UIColor clearColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        
        [label setTextColor:[UIColor whiteColor]];
        [label setTag:TAG_WAIT_SCREEN_LABEL];
        
        if ([Common is_Phone] ) {
            [label setFont:[UIFont systemFontOfSize:17.0f]];
        }
        else {
            [label setFont:[UIFont systemFontOfSize:34.0f]];
        }
        
        [subView addSubview:label];
        subView = [Common roundCornersOnView:subView onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:10.0f];
        
        [view addSubview:subView];
    }
    
    [_window addSubview:view];
}

- (void)hideWaitingScreen {
    
    UIView *view = [_window viewWithTag:TAG_WAIT_SCREEN_VIEW];
    
    if (view) {
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView*)[view viewWithTag:TAG_WAIT_SCREEN_INDICATOR];
        
        if (indicatorView)
            [indicatorView stopAnimating];
        
        [view removeFromSuperview];
        
        UILabel *label = (UILabel *)[view viewWithTag:TAG_WAIT_SCREEN_LABEL];
        if (label) {
            [label removeFromSuperview];
        }
    }
}

@end
